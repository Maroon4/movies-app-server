const express = require('express');
const io = require('socket.io');

const MovieCtrl = require('../controllers/movie-ctrl');
const UserCtrl = require('../controllers/user-ctrl');
const DialogCtrl = require('../controllers/dialog-ctrl');
const MessageCtrl = require('../controllers/message-ctrl');

// const router = express.Router(io);
const router = express.Router();

router.post('/movie', MovieCtrl.createMovie);
router.put('/movie/:id', MovieCtrl.updateMovie);
router.delete('/movie/:id', MovieCtrl.deleteMovie);
router.get('/movie/:id', MovieCtrl.getMovieById);
router.get('/movies', MovieCtrl.getMovies);


router.get('/user/me', UserCtrl.getMe );
router.post('/user/registration', UserCtrl.createUser);
router.get('/users/', UserCtrl.getUsers);
router.get('/user/:id', UserCtrl.getUserById);
router.delete('/user/:id', UserCtrl.deleteUser);
router.post('/user/login', UserCtrl.login);


router.post('/dialog/create', DialogCtrl.createDialog);
router.get('/dialog/author', DialogCtrl.getDialogByAuthor);
router.delete('/dialog/:id', DialogCtrl.deleteDialog);
router.get('/dialogs', DialogCtrl.getDialogs);

router.post('/message/create', MessageCtrl.createMessage);
router.get('/messages', MessageCtrl.getMessagesByDialogId);
router.delete('/message/:id', MessageCtrl.deleteMessage);
router.get('/allmessages', MessageCtrl.getMessages);

module.exports = router;