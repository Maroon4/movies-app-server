const express = require('express');
const socket = require('socket.io');
const {createServer} = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');


const db = require('../server/db/index');
const appRouter = require('./routs/app-router');
const updateLastSeen = require('../server/middleware/updateLastSeen')
const checkAuth = require('../server/middleware/checkAuth')



const app = express();
const http = createServer(app);
const io = socket(http);

dotenv.config();
const apiPort = process.env.PORT;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());

app.use(updateLastSeen);
app.use(checkAuth);


db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.use('/api', appRouter);
// app.use('/api', appRouter);

io.on('connection', function (socket) {
    console.log('CONNECTED');
    socket.emit('111', "sOCKETTTTTTTTTTTTTTTTTTTT");
    socket.on("CLIENT:SEND_MESSAGE", function (msg) {

        // const userId = socket.user._id;
        //
        // // console.log(req.body.dialog_id);
        //
        // const postData = {
        //     text: req.body.text,
        //     dialog: req.body.dialog_id,
        //     user: userId
        // };
        //
        // const message = new MessageModel(postData);
        //
        // console.log(message);
        //
        // if(!message) {
        //     return res.status(400).json({success: false, error: err})
        // }
        //
        // message
        //     .save()
        //     .then(() => {
        //         return res.status(201).json({
        //             success: true,
        //             id:message._id,
        //             message: "Message created"
        //         })
        //     })
        //     .catch(err => {
        //         return res.status(400).json({
        //             err,
        //             messages: "Message not created"
        //         })
        //     })
    })
});

http.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));