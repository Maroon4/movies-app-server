const jwt = require('jsonwebtoken');
const _ = require('lodash');
const {reduce} = require('lodash');

createJWToken = (user) => {

    console.log(user)
    if (typeof user !== 'object') {
        user = {}
    }

    if (!user.maxAge || typeof user.maxAge !== 'number')
    {
        user.maxAge = 3600
    }

    user.sessionData = _.reduce(user.sessionData || {}, (memo, val, key) =>
    {
        if (typeof val !== "function" && key !== "password")
        {
            memo[key] = val
        }
        return memo
    }, {})

    let token = jwt.sign({
        data: reduce(
            user,
            (result, value, key) => {
                if (key !== 'password') {
                    result[key] = value;
                }
                return result;
            },
            {}
        )
    },
        process.env.JWT_SECRET,
    {
        expiresIn: process.env.JWT_MAX_AGE,
        algorithm: 'HS256'
    })

    return token
}

module.exports = createJWToken
