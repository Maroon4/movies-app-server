const jwt = require('jsonwebtoken');

// import jwt from 'jsonwebtoken'

verifyJWTToken =  (token) =>
{
    return new Promise((resolve, reject) =>
    {
        jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) =>
        {
            if (err || !decodedToken)
            {
                return reject(err)
            }

            resolve(decodedToken)
        })
    })
};

// export default verifyJWTToken;

module.exports = verifyJWTToken
;