const {check, validationResult} = require('express-validator');

const loginValidator = [check('email'), check('password').isLength({min: 5 })];

module.exports = loginValidator;
