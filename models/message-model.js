
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const MessageSchema = new Schema({
    text: {type: String, required: true},
    dialog:{type: Schema.Types.ObjectId, ref: 'Dialog', required: true},
    user:{type: Schema.Types.ObjectId, ref: 'User'},
    unread: {type: Boolean, default: false}
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model('Message', MessageSchema);