const mongoose = require('mongoose');
// const validator = require('validator');
const {isEmail} = require("validator");
const bcrypt = require('bcrypt');

const generetePasswordHash = require('../utils/validations/generetePasswordHash');

const Schema = mongoose.Schema;


const UserSchema = new Schema(
    {
       email:{
           type: String,
           required: 'Email address is required',
           validate: [isEmail, 'Invalid email'],
           unique: true
       },
       avatar: { type: String, required: false },
       fullname: { type: String, required: true },
       password: { type: String, required: true },
       confirmed: { type: Boolean, default: false },
       confirmed_hash: { type: String, required: false },
       last_seen: { type: Date, default: new Date()}
    },
    {
        timestamps: true
    }

);

UserSchema.pre('save', function(next) {
      const user = this;

      console.log(user)

      if (!user.isModified('password')) return next();

      generetePasswordHash(user.password)
        .then(hash => {
          user.password = hash;
          next();
      }).catch(err => {
          next(err)
      })

      // bcrypt.genSalt((err, salt) => {
      //     if(err) return next(err);
      //
      //     bcrypt.hash(user.password, salt, (err, hash) => {
      //         if(err) return next(err);
      //
      //         user.password = hash;
      //         next()
      //     })
      // })
});

module.exports = mongoose.model("User", UserSchema);
