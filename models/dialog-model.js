const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DialogSchema = new Schema({
    partner: {type: Schema.Types.ObjectId, ref: 'User'},
    author: {type: Schema.Types.ObjectId, ref: 'User'},
    lastMessage:{
        type: Schema.Types.ObjectId,
        ref: 'Message',
        required: false
    }
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model('Dialog', DialogSchema);