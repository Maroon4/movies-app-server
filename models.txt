dialogs:
- _id
- author
- partner
- created_at, updated_at

messages:
- _id
- author
- partner
- text
- dialog
- unread
- created_at, updated_at

user:
- _id
- email
- fullname
- password(md5)
- confirmed
- confirmed_hash