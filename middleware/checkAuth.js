const express = require ('express');
const verifyJWTToken = require('../utils/verifyJWTToken')

checkAuth = ( req, res, next ) => {

    if(req.path === '/api/user/login' || req.path ==='/api/user/registration' || req.path === '/') {
         return next()
    }

    // console.log(req.headers.token)

    const token = req.headers.token;

    verifyJWTToken(token)
        .then(user => {
        req.user = user.data._doc
        next();
    }).catch(() => {
        res.status(400)
            .json({message: "Invalid auth token provided."})
    })


};

module.exports = checkAuth;


