const MessageModel = require('../models/message-model');

createMessage = (req, res) => {

    const body = req.body;

    console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must create a message'
        })
    }

    const userId = "5e5665ec8f42f342d41ec2eb";

    console.log(req.body.dialog_id)

    const postData = {
        text: req.body.text,
        dialog: req.body.dialog_id,
        user: userId
    }

    const message = new MessageModel(postData);

    console.log(message)

    if(!message) {
        return res.status(400).json({success: false, error: err})
    }

    message
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id:message._id,
                message: "Message created"
            })
        })
        .catch(err => {
            return res.status(400).json({
                err,
                messages: "Message not created"
            })
        })

};

getMessagesByDialogId = (req, res) => {

    // console.log(req.params.id)

    const dialogId = req.query.dialog

    console.log(dialogId)

    MessageModel.find({dialog: dialogId})
        .populate(["dialog"])
        .exec( (err, messages) => {

            console.log(messages);
            if(err) {
                return res.status(400).json({success: false, message: err})
            }

            if(!messages) {
                return res.status(404).json({success: false, message: "Message not found."})
            }

            return res.status(200).json({success: true, data: messages})

        } )
        // .catch(err => console.log(err))

};

deleteMessage = async (req, res) => {

    MessageModel.findOneAndRemove({_id: req.params.id}, (err, message) => {

        if(err) {
            return res.status(400).json({success: false, message: err})
        }

        if(!message) {
            return res.status(404).json({success: false, message: "Message not found"})
        }

        return res.status(200).json({success: true, data: message})

    }).catch(err => console.log(err))

};

getMessages = async (req, res) => {
    await MessageModel.find({}, (err, messages) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!messages.length) {
            return res
                .status(404)
                .json({ success: false, error: `Messages not found` })
        }
        return res.status(200).json({ success: true, data: messages })
    }).catch(err => console.log(err))
};

module.exports = {
  createMessage,
  getMessagesByDialogId,
  deleteMessage,
  getMessages

};