const express = require('express');
const UserModel = require('../models/user-model');
const createJWTTken = require('../utils/createJWTToken')
const generetePasswordHash = require('../utils/validations/generetePasswordHash')
const {validationResult} = require('express-validator');
const bcrypt = require('bcrypt');

createUser = (req, res) => {

    const body = req.body;

    if (!body) {
        return res.status(400).json({
            seccess: false,
            error: 'You must create a user'
        })
    }

    const user = UserModel(body)

    if(!user) {
        return res.status(400).json({success: false, error: err})
    }

    user
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: user._id,
                message: "User created"
            })
        })
        .catch(err => {
            return res.status(400).json({
                err,
                messages: "User not created"
            })
        })
};

getUserById = async (req, res) => {

    // console.log(req.params.id)

    await UserModel.findOne({_id: req.params.id}, (err, user) => {

        console.log(user)
        if(err) {
            return res.status(400).json({success: false, message: err})
        }

        if(!user) {
            return res.status(404).json({success: false, message: "User not found."})
        }

        return res.status(200).json({success: true, data: user})

    }).catch(err => console.log(err))


};

getMe = (req, res, io) => {

    console.log(req.user)
    const id = req.user._id;
    UserModel.findById(id, (err, user) => {
        if(err) {
            return res.status(404).json({
                message: 'User not found'
            });
        }
        res.json(user)
    })

}


deleteUser = async (req, res) => {

    UserModel.findOneAndDelete({_id: req.params.id}, (err, user) => {

        if(err) {
            return res.status(400).json({success: false, message: err})
        }

        if(!user) {
            return res.status(404).json({success: false, message: "User not found"})
        }

        return res.status(200).json({success: true, data: user})

    }).catch(err => console.log(err))

};

getUsers = async (req, res) => {
    await UserModel.find({}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) {
            return res
                .status(404)
                .json({ success: false, error: `Users not found` })
        }
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err))
};

login = (req, res) => {

    console.log(req.body)

    const postData = {
        email: req.body.email,
        password: req.body.password
    };

    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(422).json({errors: errors.array()});
    }

    UserModel.findOne({email: postData.email}, (err, user) => {

        console.log(user)
        if(err) {
            return res.status(404).json({
                message: "User not found"
            })
        }


    if(bcrypt.compareSync(postData.password, user.password)) {
        const token = createJWTTken(user);

        res.json({
            success: true,
            token
        })
    } else {
        res.json({
            success: false,
            message: "Incorrect password or email"
        })
    }

    //     generetePasswordHash(postData.password)
    //         .then(passwordHash => {
    //
    //         console.log(passwordHash)
    //         if(user.password === passwordHash) {
    //             const token = createJWTTken(user);
    //
    //             res.json({
    //                 success: true,
    //                 token
    //             })
    //         } else {
    //             res.json({
    //                 success: false,
    //                 message: "Incorrect password or email"
    //             })
    //         }
    //     }). catch(err => {
    //         res.status(404).json({
    //             success: false,
    //             message: err
    //         })
    //     })
    })

    // if(user.password === postData.password) {
    //        const token = createJWTTken(user);
    //
    //         res.json({
    //             success: true,
    //             token
    //         })
    //     } else {
    //         res.json({
    //             success: false,
    //             message: "Incorrect password or email"
    //         })
    //     }
    // })


}

module.exports = {
  createUser,
  getUserById,
  deleteUser,
  getUsers,
  login,
  getMe
};