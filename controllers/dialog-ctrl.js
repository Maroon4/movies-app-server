const DialogModel = require('../models/dialog-model');
const MessageModel = require('../models/message-model');

createDialog = (req, res) => {

    const body = req.body;

    console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must create a dialog'
        })
    }

    const dialog = new DialogModel(body);

    if(!dialog) {
        return res.status(400).json({success: false, error: err})
    }

    dialog
        .save()
        .then((dialogObj) => {
            const message = new MessageModel({
                text: req.body.text,
                user: req.body.author,
                dialog: dialogObj._id
            });

            message
                .save()
                .then(() => {
                    return res.status(201).json({
                        success: true,
                        id:dialog._id,
                        message: "Dialog created"
                    })
                })
        })
        .catch(err => {
            return res.status(400).json({
                err,
                messages: "Dialog not created"
            })
        })

};

getDialogByAuthor = (req, res) => {

    console.log(req.user)
    const authorId = "5e581865f7df7c18992bbeda";
    // const authorId = req.params.id

    DialogModel.find({author: authorId})
        .populate(["author", "partner"])
        .exec( (err, dialog) => {

            console.log(dialog);
            if(err) {
                return res.status(400).json({success: false, message: err})
            }

            if(!dialog) {
                return res.status(404).json({success: false, message: "Dialog not found."})
            }

            return res.status(200).json({ success: true, data: dialog })

        } )
        // .catch(err => console.log(err))

};

deleteDialog = async (req, res) => {

    DialogModel.findOneAndDelete({_id: req.params.id}, (err, dialog) => {

        if(err) {
            return res.status(400).json({success: false, message: err})
        }

        if(!user) {
            return res.status(404).json({success: false, message: "Dialog not found"})
        }

        return res.status(200).json({success: true, data: dialog})

    }).catch(err => console.log(err))

};

getDialogs = async (req, res) => {
    await DialogModel.find({}, (err, dialogs) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) {
            return res
                .status(404)
                .json({ success: false, error: `Dialogs not found` })
        }
        return res.status(200).json({ success: true, data: dialogs })
    }).catch(err => console.log(err))
};

module.exports = {
  createDialog,
  getDialogByAuthor,
  deleteDialog,
  getDialogs

};